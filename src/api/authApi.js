import request from "./request";
import { AuthUrl } from "./config";
// 权限相关api

// 登录
// const USER_LOGIN = "/login";
// 验证TOKEN
// const CHECK_TOKEN = "/apis/v1/ga/auth/token/validation";
// 认证服务-获取用户信息
const GET_USERINFO_BY_TOKEN = "/public/getUserInfoByToken";
// 认证服务-获取用户在该软件的权限信息
// const GET_AUTHINFO_BY_TOKEN = "/getAuthInfoByToken";
// 认证服务-上传该软件的权限信息
// const UPLOAD_SOFTWAREINFO = "/security/uploadBySoftwareDto";
// 验证服务器是否存在本软件的功能数据
// const CHECK_FUNCTION = "/security/checkFunction";
// 账户登录
// const ACCESS_LOGIN = "/apis/v1/ga/auth/loginWithSecurity";
// UKey登录
// const UKEY_LOGIN = "/apis/v1/ga/auth/loginWithUkey";

// auth api
export const getUserInfoByToken = () => request.post(`${GET_USERINFO_BY_TOKEN}`);

// 获取功能权限列表
export const getPermissionList = (
  headers //"apis/v1/ga/auth/software/functions/by-token";
) =>
  request.get(`/apis/v1/ga/auth/software/functions/by-token`, {
    baseURL: AuthUrl,
    headers: {
      Authorization: headers.token || "default",
      softwareFlag: headers.softwareFlag
    }
  });

export const getRoleListByToken = (params) =>
  request.get(`/userManage/rolesOfUser`, {
    baseURL: AuthUrl,
    headers: {
      Authorization: params.token || "default"
    },
    params: { yhid: params.userId }
  });
