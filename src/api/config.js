// 全局配置

export const BaseUrl =
  process.env.NODE_ENV === "development"
    ? "http://localhost:8082/apis/v1"
    : `${window.location.origin}/apis/v1`;

export const AuthUrl =
  process.env.NODE_ENV === "development"
    ? `${window.location.origin}/auth-api`
    : `${window.location.origin}/auth-api`;

export const AlarmUrl =
  process.env.NODE_ENV === "development"
    ? "http://localhost:8090/api/v1/monitoring"
    : `${window.location.origin}/api/v1/monitoring`;

export const GrafanaUrl =
  process.env.NODE_ENV === "development"
    ? "http://grafana-demo.dalianyun.com"
    : "http://grafana-demo.dalianyun.com";
