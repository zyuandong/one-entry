import Vue from 'vue'
import router from './router/router'
import ElementUI from 'element-ui'
import App from './App.vue'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/styles/common.scss'
import Axios from 'axios'

import './assets/scripts/cetc10Auth.js'

Vue.prototype.$axios = Axios;
Vue.config.productionTip = false;
Vue.use(ElementUI);

const cetc10Auth = Cetc10Auth(`${window.location.origin}/cetc10Auth.json`);
cetc10Auth.init()
  .success((resp) => {
    if (resp) {
        console.log(resp);
        new Vue({
            router,
            render: h => h(App),
        }).$mount('#app');
    } else {
      cetc10Auth.logout();
    }
  })
  .error((error) => {
    cetc10Auth.logout();
  });



// new Vue({
//     router,
//     render: h => h(App),
// }).$mount('#app');